package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class BlackThread extends ColorThread {

    private
    static final String MESSAGE = "hi blues, hi whites";
    static CountDownLatch countDownLatch;

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
        countDownLatch.countDown();
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
        // call printMessage
        printMessage();
    }
    synchronized static void setCountDownLatch(int count) {
        countDownLatch=new CountDownLatch(count);
    }
}
