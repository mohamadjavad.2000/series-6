package sbu.cs.multithread.priority;


import java.util.concurrent.CountDownLatch;

public class WhiteThread extends ColorThread {

    private
    static CountDownLatch countDownLatch;
    static final String MESSAGE = "hi back blacks, hi back blues";
    static boolean threadsShutDown =false;

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
        countDownLatch.countDown();
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
        // call printMessage
        if (!threadsShutDown) {
            try {
                BlueThread.countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            threadsShutDown=true;
        }
        printMessage();
    }
    synchronized static void setCountDownLatch(int count){
        countDownLatch=new CountDownLatch(count);
    }
}
