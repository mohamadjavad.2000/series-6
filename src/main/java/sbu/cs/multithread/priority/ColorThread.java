package sbu.cs.multithread.priority;

public abstract class ColorThread extends Thread {
    private static int massageNum=0;

    void printMessage(Message message) {
        System.out.printf("[%s] %s. thread_name: %s%n",++massageNum, message.toString(), currentThread().getName());
        Runner.addToList(message);
    }

    abstract String getMessage();
}
