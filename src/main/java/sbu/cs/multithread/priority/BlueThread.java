package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class BlueThread extends ColorThread {

    private
    static final String MESSAGE = "hi back blacks, hi whites";
    static boolean threadsShutDown =false;
    static CountDownLatch countDownLatch;

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
        countDownLatch.countDown();
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
        // call printMessage
        if (!threadsShutDown) {
            try {
                BlackThread.countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            threadsShutDown=true;
        }
        printMessage();
    }

    synchronized static void setCountDownLatch(int count) {
        countDownLatch=new CountDownLatch(count);
    }
}
