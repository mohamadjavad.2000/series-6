package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * calculate 10 ArcCot series's statements and add them to ans
 */
public class ArcCotSeriesCalc implements Runnable{
    private final BigDecimal radianX;
    private static final BigDecimal TWO = new BigDecimal("2");
    private final BigDecimal radianXPower2;
    private final int counter;
    private final BigDecimal oneDivideRadianX;
    private final Answer seriesAns;

    public ArcCotSeriesCalc(BigDecimal radianX, int counter,
                            BigDecimal oneDivideRadianX, Answer seriesAns) {
        this.radianX = radianX;
        this.counter=counter;
        radianXPower2 = radianX.pow(2);
        this.oneDivideRadianX = oneDivideRadianX;
        this.seriesAns = seriesAns;
    }

    @Override
    public void run() {
        calculate(counter);
    }
    /**
     * calculate 10 ArcCot series's statements
     * y = ArcCotangent(radian x)        0 <= y <= PI
     *
     * Formulas : ArcCot(x)=ArcTan(1/x)=ArcTan(r)= r−r^3/3+r^5/5−r^7/7+... = 1/x -1/(3*x^3) +...
     * Proves : http://specialfunctionswiki.org/index.php/Relationship_between_arctan_and_arccot
     *      & https://math.stackexchange.com/questions/29649/why-is-arctanx-x-x3-3x5-5-x7-7-dots
     *
     * @param counter power of x in ArcCot series
     */
    private void calculate(int counter) {
        BigDecimal n =new BigDecimal(counter*20+3);
        BigDecimal xPower = new BigDecimal(oneDivideRadianX.toString())
                .divide(radianX.pow(counter*20), RoundingMode.HALF_DOWN);
        BigDecimal term = null;

        boolean add = false;
        for (int i=0 ; i<10
                && ( term == null || term.compareTo(BigDecimal.ZERO) != 0 )
                ; n = n.add(TWO)) {
            //for(int i=0;i<5 &&(term=null || term!=0);n+=2){
            i++;
            xPower = xPower.divide(radianXPower2, RoundingMode.HALF_DOWN);// xp /= x^2
            term = xPower.divide(n, RoundingMode.HALF_DOWN);// term= xp/n
            seriesAns.lock();
            if (add)  seriesAns.add(term);
            else  seriesAns.subtract(term);// seriesAns+= ((-1)^(i+1))* term}
            seriesAns.unlock();
            add = ! add;
        }
        if (term.compareTo(BigDecimal.ZERO)==0){
            seriesAns.stopCalculating();
        }
    }
}
