package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PICalculator {
    private static final BigDecimal FOUR = new BigDecimal("4");
    private static final BigDecimal FIVE = new BigDecimal("5");
    private static final BigDecimal TWO_THIRTY_NINE = new BigDecimal("239");

    /**
     * calculate PI and represent it as string with given floating point number (numbers after .)
     * Formula : PI = 4*( 4*ArcCot(5) - ArcCot(239) )
     * Prove : https://www.ndl.go.jp/math/e/s1/c4_2.html
     * Check PI: till 1000 digits after floating point at https://mathshistory.st-andrews.ac.uk/HistTopics/1000_places/
     *
     * @param floatingPoint number of digits after floating point
     * @return PI in string format
     */
    public String calculate(int floatingPoint) {
        ExecutorService executor1 = Executors.newFixedThreadPool(4);
        ExecutorService executor2 = Executors.newFixedThreadPool(4);
        int numDigits = floatingPoint + 10;
        MultiThreadArcCot arcCot5=new MultiThreadArcCot(FIVE,numDigits, executor1);
        executor1.submit(arcCot5);
        MultiThreadArcCot arcCot239 =new MultiThreadArcCot(TWO_THIRTY_NINE,numDigits, executor2);
        executor2.submit(arcCot239);
        try {
            System.out.println(executor1.awaitTermination(1000, TimeUnit.MILLISECONDS));
            System.out.println(executor2.awaitTermination(1000,TimeUnit.MILLISECONDS));
        }
        catch (InterruptedException ie){
            ie.printStackTrace();
        }
        return FOUR.multiply((FOUR.multiply(arcCot5.getAns()))
                .subtract(arcCot239.getAns()))
                .setScale(floatingPoint, RoundingMode.DOWN).toString();
    }
}
