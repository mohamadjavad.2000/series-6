package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.ExecutorService;

public class MultiThreadArcCot implements Runnable{
    private final ExecutorService executor;
    private final BigDecimal oneDivideRadianX;
    private final BigDecimal radianX;
    private final Answer answer;

    /**
     * @param radianX radian input for ArcCot(x)
     * @param  numDigits number of digits of answer
     */
    public MultiThreadArcCot(BigDecimal radianX, int numDigits, ExecutorService executor) {
        this.executor = executor;
        this.radianX = radianX;

        oneDivideRadianX = BigDecimal.ONE.setScale(numDigits,
                RoundingMode.HALF_DOWN).divide(radianX, RoundingMode.HALF_DOWN);
        answer =new Answer(new BigDecimal(oneDivideRadianX.toString()));
    }
    @Override
    /*
      any Adder should calculate 5 statement and add them to sum
      if stopCalculating it will stop unusable calculation
     */
    public void run() {
        for (int counter = 0; !answer.isStopped(); counter++) {
            executor.submit(new ArcCotSeriesCalc(radianX, counter, oneDivideRadianX, answer));
            System.out.println(radianX);
        }
        executor.shutdown();
    }

    public BigDecimal getAns() {
        return answer.getAns();
    }

}
