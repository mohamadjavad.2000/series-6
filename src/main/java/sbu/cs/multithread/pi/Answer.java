package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Answer {
    private BigDecimal ans;
    private final Lock lock;

    public boolean isStopped() {
        return stopCalculating.get();
    }

    private final AtomicBoolean stopCalculating;

    public Answer(BigDecimal ans) {
        this.ans = ans;
        lock=new ReentrantLock();
        stopCalculating=new AtomicBoolean(false);
    }

    public BigDecimal getAns() {
        return ans;
    }

    public void add(BigDecimal bd) {
        ans = ans.add(bd);
    }

    public void subtract(BigDecimal bd) {
        ans = ans.subtract(bd);
    }

    public void lock() { lock.lock(); }

    public void unlock() { lock.unlock(); }

    public void stopCalculating() {
        stopCalculating.set(true);
    }
}
