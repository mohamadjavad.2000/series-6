package sbu.cs.exception;

public class BadInputException extends ApException {
    public BadInputException(String message) {
        super(" Excepted an Integer but was<"+message+"> ");
    }

    public BadInputException(int x, String arg) {
        super(" Excepted:<"+x+">but was<"+arg+"> ");
    }
}
