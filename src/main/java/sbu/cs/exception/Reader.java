package sbu.cs.exception;

import java.util.List;

import static sbu.cs.exception.Util.getImplementedCommands;
import static sbu.cs.exception.Util.getNotImplementedCommands;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args commands list
     */
    public void readTwitterCommands(List<String> args)
            throws ApException {
        List<String> implCmd=getImplementedCommands();
        List<String> notImplCmd=getNotImplementedCommands();
        for (String arg: args) {
            if (!implCmd.contains(arg)) {
                if (notImplCmd.contains(arg))
                    throw new NotImplementedCommandException(arg);
                else throw new UnrecognizedCommandException(arg);
            }
        }
    }

    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args inputs
     */
    public void read(String...args) throws BadInputException {
        int x;
        for (int i = 0; i <args.length; i++) {
            System.out.println(args[i]);
        }
        for (int i = 1; i < args.length; i+=2) {
            try {
                x=Integer.parseInt(args[i]);
            }catch (NumberFormatException formatException){
                throw new BadInputException(args[i]);
            }
            if (!Integer.toString(x).equals(args[i])){
                throw new BadInputException(x,args[i]);
            }
        }
    }
}
