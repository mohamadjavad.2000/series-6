package sbu.cs.exception;

public class NotImplementedCommandException extends ApException {
    public NotImplementedCommandException(String cmd) {
        super(" NotImplementedCommand {"+cmd+"} ");
    }
}
